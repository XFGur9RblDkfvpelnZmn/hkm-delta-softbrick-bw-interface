using System;
using System.Globalization;
namespace HKM.MaakBwFile
{
	public class WeekToDate
	{
		public static DateTime FirstDateOfWeek(int year, int weekNum)
		{
			CalendarWeekRule rule = CalendarWeekRule.FirstFourDayWeek;
			DateTime time = new DateTime(year, 1, 1);
			int num = (int)(DayOfWeek.Monday - time.DayOfWeek);
			DateTime dateTime = time.AddDays((double)num);
			Calendar calendar = CultureInfo.InvariantCulture.Calendar;
			int weekOfYear = calendar.GetWeekOfYear(time, rule, DayOfWeek.Monday);
			if (weekOfYear <= 1)
			{
				weekNum--;
			}
			return dateTime.AddDays((double)(weekNum * 7));
		}
	}
}
