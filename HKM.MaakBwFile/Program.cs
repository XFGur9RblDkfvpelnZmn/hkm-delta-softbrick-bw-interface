using Algemeen;
using HKM.MaakBwFile.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
namespace HKM.MaakBwFile
{
	public class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				if (args.Length >= 3)
				{
					string path = args[2];
					string text = string.Empty;
					DateTime dateTime = DateTime.Now;
					string text2 = string.Empty;
					DateTime dateTime2 = DateTime.Now;
					int num;
					if (int.TryParse(args[0], out num))
					{
						text = Proces.dtStringToDate(args[0]).ToString("yyMMdd");
						dateTime = Proces.dtStringToDate(args[0]);
					}
					else
					{
						text = args[0];
					}
					if (int.TryParse(args[1], out num))
					{
						text2 = Proces.dtStringToDate(args[1]).ToString("yyMMdd");
						dateTime2 = Proces.dtStringToDate(args[1]);
					}
					else
					{
						text2 = args[1];
					}
					List<int> list = new List<int>();
					list.Add(0);
					list.Add(2);
					Dictionary<string, List<string>> dictionary = Proces.DlsCsvToDictionary(new FileInfo(Settings.Default.HCcodeFile), list);
					Dictionary<string, List<string>> dictionary2 = Proces.DlsCsvToDictionary(new FileInfo(Settings.Default.ValutaFile), list);
					DateTime dateTime3 = dateTime;
					Dictionary<string, Dictionary<string, List<string>>> dictionary3 = new Dictionary<string, Dictionary<string, List<string>>>();
					while (dateTime3 <= dateTime2)
					{
						dictionary3.Add(dateTime3.ToString("yyMMdd"), Proces.DlsSqlSel("badnum, salnum, wplekkod, mandant, price, sdatp, edatp from personeel", dateTime3));
						dateTime3 = dateTime3.AddDays(1.0);
					}
					string sSqlCommando = string.Concat(new string[]
					{
						"badnum, van_date, van_time, time, wplekkod, filler, actkod from proj_regi where plan = '1' and van_date >= '",
						text,
						"' and van_date <= '",
						text2,
						"' and actkod < 100"
					});
					List<string> list2 = new List<string>();
					list2.Add("-d;");
					FileInfo fiCsvFile = Proces.FiSqlsel(sSqlCommando, list2);
					List<List<string>> list3 = Proces.LlsCsvToList(fiCsvFile);
					
					List<List<string>> list4 = list3.FindAll(delegate(List<string> ls)
					{
						int num45;
						return ls.Count >= 6 && int.TryParse(ls[5].Trim(), out num45) && num45 > 0;
					});
					List<List<string>> list5 = new List<List<string>>();
					foreach (List<string> current in list4)
					{
						int num2 = 0;
						int num3 = 0;
						int num4 = 0;
						int num5 = 0;
						if (int.TryParse(current[1].Trim(), out num5) && int.TryParse(current[2].Trim(), out num2) && int.TryParse(current[3].Trim(), out num3) && int.TryParse(current[5].Trim(), out num4))
						{
							int num6 = num3 / 2;
							num6 -= num6 % 15;
							int num7 = num6 + num2 + num4;
							int num8 = num3 - num6;
							List<string> list6 = new List<string>();
							list6.Add(current[0]);
							list6.Add(current[1]);
							list6.Add(current[2]);
							list6.Add(num6.ToString());
							list6.Add(current[4]);
							list6.Add("0");
							list6.Add(current[6]);
							if (num7 > 1440)
							{
								num7 -= 1440;
								string format = "yyMMdd";
								if (num5 > 20000000)
								{
									format = "yyyyMMdd";
								}
								current[1] = DateTime.ParseExact(num5.ToString(), format, null).AddDays(1.0).ToString("yyMMdd");
							}
							List<string> list7 = new List<string>();
							list7.Add(current[0]);
							list7.Add(current[1]);
							list7.Add(num7.ToString());
							list7.Add(num8.ToString());
							list7.Add(current[4]);
							list7.Add("0");
							list7.Add(current[6]);
							list5.Add(list6);
							list5.Add(list7);
						}
						else
						{
							string text3 = "Wrong record:";
							foreach (string current2 in current)
							{
								text3 = text3 + current2 + ";";
							}
							Console.WriteLine(text3);
							Log.Verwerking(text3);
						}
					}
					List<List<string>> collection = list3.FindAll(delegate(List<string> ls)
					{
						int num45;
						int num46;
						return ls.Count < 6 || !int.TryParse(ls[5].Trim(), out num45) || !int.TryParse(ls[3].Trim(), out num46) || (num45 <= 0 && num46 != 0);
					});
					list5.AddRange(collection);
					CultureInfo cultureInfo = new CultureInfo("nl-BE");
					Calendar calendar = cultureInfo.Calendar;
					CalendarWeekRule arg_45A_0 = cultureInfo.DateTimeFormat.CalendarWeekRule;
					DayOfWeek firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
					string text4 = dateTime.ToString("yy") + calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstFourDayWeek, firstDayOfWeek).ToString("00");
					string text5 = dateTime2.ToString("yy") + calendar.GetWeekOfYear(dateTime2, CalendarWeekRule.FirstFourDayWeek, firstDayOfWeek);
					string sSqlCommando2 = string.Concat(new string[]
					{
						"badnum, week, ucat[0], ucat[1], ucat[2], ucat[3], ucat[4], ucat[5], ucat[6], sollzeit[0], sollzeit[1], sollzeit[2], sollzeit[3], sollzeit[4], sollzeit[5], sollzeit[6] from afwgmz where week >= '",
						text4,
						"' and week <= '",
						text5,
						"'"
					});
					FileInfo fiCsvFile2 = Proces.FiSqlsel(sSqlCommando2, list2);
					List<List<string>> list8 = Proces.LlsCsvToList(fiCsvFile2);
					foreach (List<string> current3 in list8)
					{
						if (current3.Count >= 17)
						{
							string s = "20" + current3[1];
							int num9 = 0;
							int.TryParse(s, out num9);
							int num10 = num9 % 100;
							num9 = (num9 - num10) / 100;
							if (num9 >= 2010 && num10 >= 1 && num10 <= 54)
							{
								DateTime dateTime4 = WeekToDate.FirstDateOfWeek(num9, num10);
								for (int i = 0; i <= 6; i++)
								{
									DateTime dateTime5 = dateTime4.AddDays((double)i);
									int num11 = 0;
									if (current3.Count >= 10 + i && current3[2 + i].Trim().Length > 0 && current3[9 + i].Trim().Length > 0 && int.TryParse(current3[9 + i], out num11) && num11 > 0)
									{
										list5.Add(new List<string>
										{
											current3[0],
											dateTime5.ToString("yyMMdd"),
											"0",
											num11.ToString(),
											"",
											"0",
											current3[2 + i]
										});
									}
								}
							}
							else
							{
								string text6 = "Afwgmz record invalid. Incorrect week number." + Environment.NewLine;
								text6 = text6 + "Record was: " + Environment.NewLine;
								foreach (string current4 in current3)
								{
									text6 = text6 + current4 + ";";
								}
								Log.Verwerking(text6);
							}
						}
					}
					string text7 = dateTime2.ToString("yyMMdd");
					if (dateTime2 > DateTime.Now)
					{
						text7 = DateTime.Now.ToString("yyMMdd");
					}
					FileInfo fiFile = Proces.FiSqlsel(string.Concat(new string[]
					{
						"badnum, actkod, van_date, van_time, time,filler from proj_regi where van_date <= '",
						text7,
						"' and van_date >= '",
						text,
						"' and (time > 0 or actkod = '999') and projkod = ''"
					}), list2);
					FileInfo fiFile2 = Proces.FiSqlsel(string.Concat(new string[]
					{
						"badnum, date, time, stat from pdreg where date <= '",
						text7,
						"' and date >= '",
						text,
						"' and ucat <> '-1'"
					}), list2);
					FileInfo fiFile3 = Proces.FiSqlsel(string.Concat(new string[]
					{
						"badnum, date, time, ucat, wplekkod from lewkucat where date <= '",
						text7,
						"' and date >= '",
						text,
						"' and ucat <> '-1'"
					}), list2);
					int num12 = 5;
					int num13 = 5;
					List<List<string>> list9 = new List<List<string>>();
					List<List<string>> list10 = new List<List<string>>();
					string[] array = Regex.Split(Proces.SLeesFile(fiFile), Environment.NewLine);
					for (int j = 0; j < array.Length; j++)
					{
						string text8 = array[j];
						string[] array2 = text8.Split(new char[]
						{
							';'
						});
						if (array2.Length >= 6)
						{
							string item = array2[0];
							string item2 = array2[1];
							string item3 = array2[2];
							string item4 = array2[3];
							string item5 = array2[4];
							string item6 = array2[5];
							list9.Add(new List<string>
							{
								item,
								item2,
								item3,
								item4,
								item5,
								"P",
								item6
							});
						}
					}
					array = Regex.Split(Proces.SLeesFile(fiFile2), Environment.NewLine);
					for (int j = 0; j < array.Length; j++)
					{
						string text9 = array[j];
						string[] array3 = text9.Split(new char[]
						{
							';'
						});
						if (array3.Length >= 4)
						{
							string item7 = array3[0];
							string item8 = array3[1];
							string item9 = array3[2];
							string item10 = array3[3];
							list9.Add(new List<string>
							{
								item7,
								"",
								item8,
								item9,
								item10,
								"K"
							});
						}
					}
                    for (int i = 0; i < list9.Count-1;i++ )
                    {
                        if (list9[i][4] == "I" && list9[i + 1][4] == "I")
                        {
                            list9[i] = new List<string>
							{
								"",
								"",
								"",
								"",
								"",
								""
							};

                        }
                    }
					array = Regex.Split(Proces.SLeesFile(fiFile3), Environment.NewLine);
					for (int j = 0; j < array.Length; j++)
					{
						string text10 = array[j];
						string[] array4 = text10.Split(new char[]
						{
							';'
						});
						if (array4.Length >= 6)
						{
							string item11 = array4[0];
							string item12 = array4[3];
							string item13 = array4[1];
							string item14 = "00";
							string item15 = array4[2];
							string item16 = "00";
							string item17 = array4[4];
							list10.Add(new List<string>
							{
								item11,
								item12,
								item13,
								item14,
								item15,
								"U",
								item16,
								item17
							});
						}
					}
					list9.Sort(delegate(List<string> ls1, List<string> ls2)
					{
						int num45 = ls1[0].CompareTo(ls2[0]);
						if (num45 != 0)
						{
							return num45;
						}
						num45 = ls1[2].CompareTo(ls2[2]);
						if (num45 != 0)
						{
							return num45;
						}
						num45 = ls1[3].CompareTo(ls2[3]);
						if (num45 != 0)
						{
							return num45;
						}
						return ls2[5].CompareTo(ls1[5]);
					});
					List<List<string>> list11 = new List<List<string>>();
					List<string> list12 = null;
					new Dictionary<string, int>();
					foreach (List<string> ls in list9)
					{
						int iTmpPlanDuur = 0;
						List<string> list13 = list9.Find((List<string> lsRec) => lsRec[0] == ls[0] && lsRec[2] == ls[2] && int.TryParse(lsRec[4], out iTmpPlanDuur) && iTmpPlanDuur > 0 && lsRec[1].Trim() != "999");
						int num14 = 0;
						int num15 = 0;
						if (list13 != null)
						{
							int.TryParse(list13[3], out num14);
							int.TryParse(list13[6], out num15);
						}
						if (ls[5] == "P")
						{
							if (list13 != null)
							{
								int num16 = 0;
								if (int.TryParse(ls[3], out num16) && (num16 < num14 || num16 > iTmpPlanDuur + num14 + num15))
								{
									int num17 = 0;
									if (int.TryParse(ls[4], out num17) && num17 > 0 && ls[1].Trim() == "999")
									{
										list11.Add(new List<string>
										{
											ls[0],
											ls[2],
											ls[3],
											ls[4],
											"",
											"0",
											"0"
										});
									}
								}
							}
							else
							{
								int num18 = 0;
								if (int.TryParse(ls[4], out num18) && num18 > 0 && ls[1].Trim() == "999")
								{
									list11.Add(new List<string>
									{
										ls[0],
										ls[2],
										ls[3],
										ls[4],
										"",
										"0",
										"0"
									});
								}
							}
						}
						else
						{
							if (ls[5] == "K" && (ls[4].Trim() == "I" || ls[4].Trim() == "d"))
							{
								int iTmpKlok = 0;
								int iTmpLs = 0;
								if (list13 != null)
								{
									List<string> list14 = list9.Find((List<string> lsRec) => lsRec[0] == ls[0] && lsRec[2] == ls[2] && lsRec[5] == "K" && (lsRec[4].Trim() == "U" || lsRec[4].Trim() == "d") && int.TryParse(lsRec[3], out iTmpKlok) && int.TryParse(ls[3], out iTmpLs) && iTmpKlok > iTmpLs && lsRec[5] == "K");
									if (list14 != null)
									{
										int num19 = iTmpLs;
										int num20 = iTmpKlok;
										if (num19 <= num14 - num12)
										{
											num19 = num14;
										}
										else
										{
                                            int iLaatstVerwerktUit = 999999;
                                            if (list12 != null)
                                            {
                                                Int32.TryParse(list12[3], out iLaatstVerwerktUit);
                                            }
                                            if ((num19 >= num14 + iTmpPlanDuur + num15) && (list12 == null || num14 + iTmpPlanDuur + num15 <= iLaatstVerwerktUit))
											{
												num19 = num14 + iTmpPlanDuur + num15;
											}
											else
											{
                                                if (num19 > num14 && num19 <= num14 + num12 && (list12 == null || num14 + iTmpPlanDuur + num15 <= iLaatstVerwerktUit))
												{
													num19 = num14;
												}
											}
										}
										if (list12 != null && list12[0] == ls[0] && list12[2] == ls[2])
										{
											int num21 = 0;
											int.TryParse(list12[3], out num21);
											if (num19 - num21 > num15 && num19 - num21 <= num15 + 5)
											{
												num19 = num21 + num15;
											}
											else
											{
												if (num19 - num21 < num15 && num19 - num21 >= num15 - 5)
												{
													num19 = num21 + num15;
												}
											}
                                            if (num19 - num21 <=5)
                                                num19 = 999999;
										}
										if (num20 >= num14 + iTmpPlanDuur + num15 - num13)
										{
											num20 = num14 + iTmpPlanDuur + num15;
										}
										list14[4] = "X";
										list12 = list14;
                                        if (num20 > num19)
                                        {
                                            list11.Add(new List<string>
										    {
											    ls[0],
											    ls[2],
											    num19.ToString(),
											    (num20 - num19).ToString(),
											    "",
											    "",
											    "0",
											    ls[4].Trim()
										    });
                                        }
									}
								}
							}
						}
					}
					foreach (List<string> ls in list10)
					{
						if (ls[1].Trim() == "94")
						{
							int k = 0;
							if (int.TryParse(ls[4], out k))
							{
								List<List<string>> list15 = list11.FindAll(delegate(List<string> lsRec)
								{
									string a = "";
									if (lsRec.Count > 7)
									{
										//a = lsRec[7];
									}
									return lsRec[0] == ls[0] && lsRec[1] == ls[2].Substring(2) && lsRec[6] == "0" && a == "";
								});
								if (list15 != null)
								{
									while (k > 0)
									{
										List<string> list16 = list15.Find(delegate(List<string> lsRec)
										{
											int num45 = 0;
											int.TryParse(lsRec[3], out num45);
											return num45 > 0;
										});
										if (list16 != null)
										{
											int num22 = 0;
											int.TryParse(list16[3], out num22);
											if (num22 >= k)
											{
												num22 -= k;
												k = 0;
											}
											else
											{
												k -= num22;
												num22 = 0;
											}
											list16[3] = num22.ToString();
										}
										else
										{
											k = 0;
										}
									}
								}
							}
						}
						list11.Add(new List<string>
						{
							ls[0],
							ls[2],
							ls[3],
							ls[4],
							ls[7],
							"",
							ls[1]
						});
					}
					using (StreamWriter streamWriter = new StreamWriter(path))
					{
						foreach (List<string> current5 in list5)
						{
							if (current5.Count > 1 && dictionary3.ContainsKey(Proces.dtStringToDate(current5[1]).ToString("yyMMdd")) && dictionary3[Proces.dtStringToDate(current5[1]).ToString("yyMMdd")].ContainsKey(current5[0]))
							{
								List<string> list17 = dictionary3[Proces.dtStringToDate(current5[1]).ToString("yyMMdd")][current5[0]];
								if (Proces.dtStringToDate(current5[1]) >= Proces.dtStringToDate(list17[4]) && Proces.dtStringToDate(current5[1]) <= Proces.dtStringToDate(list17[5]))
								{
									string arg_123A_0 = list17[0];
									string text11 = current5[0];
									decimal d = 0m;
									decimal.TryParse(list17[3], out d);
									(d / 100m).ToString("00.00");
									string text12 = current5[6];
									if (text12.Trim() == string.Empty)
									{
										text12 = "01";
									}
									string text13 = "00";
									if (dictionary.ContainsKey(text12 + ";" + list17[2] + ";"))
									{
										text13 = dictionary[text12 + ";" + list17[2] + ";"][4];
									}
									string text14 = "EUR";
									if (dictionary2.ContainsKey(list17[2] + ";"))
									{
										text14 = dictionary2[list17[2] + ";"][1];
									}
									string text15 = string.Empty;
									if (current5[4].Trim().Length > 0)
									{
										text15 = current5[4].Trim();
									}
									else
									{
										text15 = list17[1];
									}
									int num23 = 0;
									int num24 = 0;
									if (int.TryParse(current5[2].Trim(), out num23) && int.TryParse(current5[3].Trim(), out num24))
									{
										int num25 = num23 / 60;
										int num26 = num23 % 60;
										int num27;
										int num28;
										if (num23 != 0)
										{
											num27 = (num23 + num24) / 60;
											num28 = (num23 + num24) % 60;
										}
										else
										{
											num27 = 0;
											num28 = 0;
										}
										int num29 = num24 / 60;
										int num30 = Convert.ToInt32(Convert.ToDecimal(num24 % 60) / 60m * 100m);
										if (num27 < 24 && (num29>0||num30>00))
										{
											streamWriter.WriteLine(string.Concat(new string[]
											{
												text12,
												";",
												text13,
												";",
												text11,
												";20",
												current5[1],
												";",
												text15,
												";",
												num25.ToString(),
												":",
												num26.ToString("00"),
												":00;",
												num27.ToString(),
												":",
												num28.ToString("00"),
												":00;",
												num29.ToString(),
												",",
												num30.ToString("00"),
												";;",
												text14,
												";20;0;"
											}));
										}
										else
										{
                                            if (num27 == 24 && num28 == 0 && (num29 > 0 || num30 > 00))
											{
												num27 = 0;
												num28 = 0;
												streamWriter.WriteLine(string.Concat(new string[]
												{
													text12,
													";",
													text13,
													";",
													text11,
													";20",
													current5[1],
													";",
													text15,
													";",
													num25.ToString(),
													":",
													num26.ToString("00"),
													":00;",
													num27.ToString(),
													":",
													num28.ToString("00"),
													":00;",
													num29.ToString(),
													",",
													num30.ToString("00"),
													";;",
													text14,
													";20;0;"
												}));
											}
											else 
											{
												if (num27 == 24)
												{
													num27 = 0;
												}
												int num31 = num27;
												int num32 = num28;
												num27 = 0;
												num28 = 0;
												num24 = 1440 - num23;
												num29 = num24 / 60;
												num30 = Convert.ToInt32(Convert.ToDecimal(num24 % 60) / 60m * 100m);
                                                int num33 = 20130101; 
                                                if (num29 > 0 || num30 > 00)
                                                {
                                                   
                                                    int.TryParse(current5[1], out num33);
                                                    streamWriter.WriteLine(string.Concat(new string[]
												    {
													    text12,
													    ";",
													    text13,
													    ";",
													    text11,
													    ";20",
													    num33.ToString(),
													    ";",
													    text15,
													    ";",
													    num25.ToString(),
													    ":",
													    num26.ToString("00"),
													    ":00;",
													    num27.ToString(),
													    ":",
													    num28.ToString("00"),
													    ":00;",
													    num29.ToString(),
													    ",",
													    num30.ToString("00"),
													    ";;",
													    text14,
													    ";20;0;"
												    }));
                                                }
												string format2 = "yyMMdd";
												if (num33 > 20000000)
												{
													format2 = "yyyyMMdd";
												}
												DateTime t = DateTime.ParseExact(num33.ToString(), format2, null).AddDays(1.0);
												num33 = Convert.ToInt32(t.ToString("yyyyMMdd"));
												if (t >= Proces.dtStringToDate(list17[4]) && t <= Proces.dtStringToDate(list17[5]))
												{
													if (num31 >= 24)
													{
														num31 -= 24;
													}
													num25 = 0;
													num26 = 0;
													num27 = num31;
													num28 = num32;
													num29 = num27;
													num30 = Convert.ToInt32(Convert.ToDecimal(num28) / 60m * 100m);
                                                    if (t.Ticks >= dateTime.Ticks && t.Ticks <= dateTime2.Ticks && (num29 > 0 || num30 > 00))
													{
														streamWriter.WriteLine(string.Concat(new string[]
														{
															text12,
															";",
															text13,
															";",
															text11,
															";",
															num33.ToString(),
															";",
															text15,
															";",
															num25.ToString(),
															":",
															num26.ToString("00"),
															":00;",
															num27.ToString(),
															":",
															num28.ToString("00"),
															":00;",
															num29.ToString(),
															",",
															num30.ToString("00"),
															";;",
															text14,
															";20;0;"
														}));
													}
												}
											}
										}
									}
									else
									{
										string text16 = "Incorrect data in BW Record, starttime or time incorrect format" + Environment.NewLine;
										text16 += "Record: ";
										foreach (string current6 in current5)
										{
											text16 = text16 + current6 + ";";
										}
										Log.Verwerking(text16);
										Console.WriteLine(text16);
									}
								}
							}
							else
							{
								string sBericht = "Employee data not found with badnum " + current5[0];
								Log.Verwerking(sBericht);
							}
						}
						foreach (List<string> current7 in list11)
						{
							if (current7.Count > 1 && dictionary3.ContainsKey(Proces.dtStringToDate(current7[1]).ToString("yyMMdd")) && dictionary3[Proces.dtStringToDate(current7[1]).ToString("yyMMdd")].ContainsKey(current7[0]))
							{
								List<string> list18 = dictionary3[Proces.dtStringToDate(current7[1]).ToString("yyMMdd")][current7[0]];
								if (list18[2].Trim() == "dis")
								{
									string arg_1B2E_0 = list18[0];
									string text17 = current7[0];
									decimal d2 = 0m;
									decimal.TryParse(list18[3], out d2);
									(d2 / 100m).ToString("00.00");
									string text18 = current7[6];
									if (text18.Trim() == string.Empty || text18.Trim() == "0")
									{
										text18 = "01";
									}
									string text19 = "00";
									if (dictionary.ContainsKey(text18 + ";" + list18[2] + ";"))
									{
										text19 = dictionary[text18 + ";" + list18[2] + ";"][4];
									}
									string text20 = "EUR";
									if (dictionary2.ContainsKey(list18[2] + ";"))
									{
										text20 = dictionary2[list18[2] + ";"][1];
									}
									string text21 = string.Empty;
									if (current7[4].Trim().Length > 0)
									{
										text21 = current7[4].Trim();
									}
									else
									{
										text21 = list18[1];
									}
									string text22 = current7[1].Trim();
									if (text22.Length == 6)
									{
										text22 = "20" + text22;
									}
									int num34 = 0;
									int num35 = 0;
									if (int.TryParse(current7[2].Trim(), out num34) && int.TryParse(current7[3].Trim(), out num35))
									{
										int num36 = num34 / 60;
										int num37 = num34 % 60;
										int num38;
										int num39;
										if (num34 != 0)
										{
											num38 = (num34 + num35) / 60;
											num39 = (num34 + num35) % 60;
										}
										else
										{
											num38 = 0;
											num39 = 0;
										}
										int num40 = num35 / 60;
										int num41 = Convert.ToInt32(Convert.ToDecimal(num35 % 60) / 60m * 100m);
										if (num40 > 0 || num41 > 0)
										{
                                            if (num38 < 24 && (num40 > 0 || num41 > 00))
											{
												streamWriter.WriteLine(string.Concat(new string[]
												{
													text18,
													";",
													text19,
													";",
													text17,
													";",
													text22,
													";",
													text21,
													";",
													num36.ToString(),
													":",
													num37.ToString("00"),
													":00;",
													num38.ToString(),
													":",
													num39.ToString("00"),
													":00;",
													num40.ToString(),
													",",
													num41.ToString("00"),
													";;",
													text20,
													";10;0;"
												}));
											}
											else
											{
                                                if (num38 == 24 && num39 == 0 && (num40 > 0 || num41 > 00))
												{
													num38 = 0;
													num39 = 0;
													streamWriter.WriteLine(string.Concat(new string[]
													{
														text18,
														";",
														text19,
														";",
														text17,
														";",
														text22,
														";",
														text21,
														";",
														num36.ToString(),
														":",
														num37.ToString("00"),
														":00;",
														num38.ToString(),
														":",
														num39.ToString("00"),
														":00;",
														num40.ToString(),
														",",
														num41.ToString("00"),
														";;",
														text20,
														";10;0;"
													}));
												}
												else
												{
													if (num38 == 24)
													{
														num38 = 0;
													}
													int num42 = num38;
													int num43 = num39;
													num38 = 0;
													num39 = 0;
													num35 = 1440 - num34;
													num40 = num35 / 60;
													num41 = Convert.ToInt32(Convert.ToDecimal(num35 % 60) / 60m * 100m);
													int num44 = 20130101;
													int.TryParse(text22, out num44);
                                                    if (num40 > 0 || num41 > 0)
                                                    {
                                                        streamWriter.WriteLine(string.Concat(new string[]
													    {
														    text18,
														    ";",
														    text19,
														    ";",
														    text17,
														    ";",
														    num44.ToString(),
														    ";",
														    text21,
														    ";",
														    num36.ToString(),
														    ":",
														    num37.ToString("00"),
														    ":00;",
														    num38.ToString(),
														    ":",
														    num39.ToString("00"),
														    ":00;",
														    num40.ToString(),
														    ",",
														    num41.ToString("00"),
														    ";;",
														    text20,
														    ";10;0;"
													    }));
                                                    }
													string format3 = "yyMMdd";
													if (num44 > 20000000)
													{
														format3 = "yyyyMMdd";
													}
													num44 = Convert.ToInt32(DateTime.ParseExact(num44.ToString(), format3, null).AddDays(1.0).ToString("yyyyMMdd"));
													num36 = 0;
													num37 = 0;
													if (num42 >= 24)
													{
														num42 -= 24;
													}
													num38 = num42;
													num39 = num43;
													num40 = num38;
													num41 = Convert.ToInt32(Convert.ToDecimal(num39) / 60m * 100m);
                                                    if (num40 > 0 || num41 > 0)
                                                    {
                                                        streamWriter.WriteLine(string.Concat(new string[]
													    {
														    text18,
														    ";",
														    text19,
														    ";",
														    text17,
														    ";",
														    num44.ToString(),
														    ";",
														    text21,
														    ";",
														    num36.ToString(),
														    ":",
														    num37.ToString("00"),
														    ":00;",
														    num38.ToString(),
														    ":",
														    num39.ToString("00"),
														    ":00;",
														    num40.ToString(),
														    ",",
														    num41.ToString("00"),
														    ";;",
														    text20,
														    ";10;0;"
													    }));
                                                    }
												}
											}
										}
									}
									else
									{
										string text23 = "Incorrect data in BW Record, starttime or time incorrect format" + Environment.NewLine;
										text23 += "Record: ";
										foreach (string current8 in current7)
										{
											text23 = text23 + current8 + ";";
										}
										Log.Verwerking(text23);
										Console.WriteLine(text23);
									}
								}
							}
							else
							{
								string text24 = "Employee data not found with badnum " + current7[0];
								Console.WriteLine(text24);
								Log.Verwerking(text24);
							}
						}
						goto IL_23B0;
					}
				}
				Console.WriteLine("This process needs to be called with 3 parameters (startdate, enddate and export file).");
				Console.WriteLine("Only " + args.Length + " arguments were supplied.");
				string text25 = "Process HKM.MaakBwDelta needs to be called with 3 parameters (startdate, enddate and export file)" + Environment.NewLine + "The arguments were: ";
				for (int j = 0; j < args.Length; j++)
				{
					string str = args[j];
					text25 = text25 + " " + str;
				}
				Log.Verwerking(text25);
				IL_23B0:;
			}
			catch (Exception ex)
			{
				Console.Write("Unhandled exception:" + Environment.NewLine);
				Console.Write(ex.ToString());
				Log.Exception(ex);
			}
			finally
			{
				Proces.VerwijderWrkFiles();
			}
		}
	}
}
