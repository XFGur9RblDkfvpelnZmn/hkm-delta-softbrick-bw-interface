using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
namespace HKM.MaakBwFile.Properties
{
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"), CompilerGenerated]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}
		[DefaultSettingValue("C:\\Users\\pdoo\\Documents\\visual studio 2010\\Projects\\ismatopius-hkm-delta-softbrick-bw-interface-9d834cea3f41\\release\\hccode2.csv"), UserScopedSetting, DebuggerNonUserCode]
		public string HCcodeFile
		{
			get
			{
				return (string)this["HCcodeFile"];
			}
			set
			{
				this["HCcodeFile"] = value;
			}
		}
		[DefaultSettingValue("C:\\Users\\pdoo\\Documents\\visual studio 2010\\Projects\\ismatopius-hkm-delta-softbrick-bw-interface-9d834cea3f41\\release\\valuta.csv"), UserScopedSetting, DebuggerNonUserCode]
		public string ValutaFile
		{
			get
			{
				return (string)this["ValutaFile"];
			}
			set
			{
				this["ValutaFile"] = value;
			}
		}
	}
}
